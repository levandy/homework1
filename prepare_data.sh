#/bin/bash

# ./prepare_data.sh -l http://bunwell.cs.ucl.ac.uk/ipinyou.contest.dataset.zip -s 19 -f 27 -i input -m city.en.txt -t imp.201310 -d training3rd
# 1 - link to zip
# 2 - subdir in zip
# 3 - template
# 4 - min in range XX-YY of ZIP/SUBDIR/TEMPLATE[XX-YY].txt.bz2 to extract
# 5 - max in range XX-YY of ZIP/SUBDIR/TEMPLATE[XX-YY].txt.bz2 to extract
# 6 - input folder for hadoop
# 7 - mapping file name
log() {
now=$(date +"%T");
echo "$now ---> $1";
}

usage() {
echo "
-------------------------------
Usage: $0
[-l <link>]
[-d <dir in zip>]
[-t <template file in subdir>]
[-s <min in range>]
[-f <max in range>]
[-i <input directory>]
[-m <mapping filename>]
ALL PARAMS ARE REQUIRED!
-------------------------------
";
exit 1; }

while getopts ":l:d:t:i:m:s:f:" option;
do
	case $option
	in
	l) link=${OPTARG};;
	d) subdirectory=${OPTARG};;
	t) template=${OPTARG};;
	s) start=${OPTARG};;
	f) finish=${OPTARG};;
	i) input=${OPTARG};;
	m) map=${OPTARG};;
	*) usage;;
	esac
done

#wget "$link";
log "File $link was downloaded!"
# change to hdfs commands
hadoop fs -mkdir -p "$input";
hadoop fs -mkdir -p "$output";
log "Directories were created!"

# get just filename with extension
url=$(basename $link)
# get filename without extension
directory=${url##*/}
directory=${directory%.*}
# full path to .txt.bz2 files
path="$directory/$subdirectory/$template"

log "Beginning or unzipping..."
unzip "$url"
log "Archive was unzipped to $directory"
for (( i=$start; i<=$finish; i++));
do
	bzip2 -d "$path$i.txt.bz2";
	log "$path$i.txt.bz2 unzipped!"
	#hdfs fs -put "$path$i.txt" "$input"
	hadoop fs -put "$path$i.txt" "$input";
	log "$path$i.txt put in $input!"
done
hadoop fs -put "$directory/$map";
rm -rf $directory;
rm -rf "~/.local/share/Trash/$directory";
log "All files were copied to $input folder"
