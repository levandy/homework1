# Homework 1
DS:BDA Homework 1 repo
### Prerequisites (Cloudera)
- 64-bit VMs require a 64-bit host OS and a virtualization product that can support a 64-bit guest OS.
- The amount of RAM required by the VM: 8+ GiB 
### How To
Build project: 
```sh
$ git clone git@bitbucket.org:levandy/homework1.git
$ cd homework1
$ mvn clean install
```
Prepare data:
```sh
$ chmod +x prepare_data.sh
$ ./prepare_data.sh -l http://bunwell.cs.ucl.ac.uk/ipinyou.contest.dataset.zip -s 19 -f 27 -i input -m city.en.txt -t imp.201310 -d training3rd
```
Run:
```sh
$ cd target
$ hadoop jar homework1-0.0.1.jar ru.levandy.mephi.Driver input output
```
Show output:
```sh
$ hadoop fs -text output/*
```