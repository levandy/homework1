package ru.levandy.mephi;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class PricePartitioner extends Partitioner <Text, IntWritable> {
    /**
     * Logic: get OS name and return number of partition for key-value according to OS type
     * @param key city name
     * @param value price value
     * @param i total num of partitions
     * @return number of partition
     */
    @Override
    public int getPartition(Text key, IntWritable value, int i) {
        if (i == 0) return 0;
        String os = System.getProperty("os.name").toLowerCase();
        return isWindows(os) ? 0 : isMac(os) ? 1 % i : isUnix(os) ? 2 % i : isSolaris(os) ? 3 % i : 4 % i;
    }

    private boolean isWindows(String os) {
        return (os.contains("win"));
    }

    private boolean isMac(String os) {
        return (os.contains("mac"));
    }

    private boolean isUnix(String os) {
        return (os.contains("nix") || os.contains("nux") || os.indexOf("aix") > 0 );
    }

    private boolean isSolaris(String os) {
        return (os.contains("sunos"));
    }
}
