package ru.levandy.mephi;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PriceReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    /**
     * Logic: iterate price values,
     * if value > 250 -> increment counter,
     * write data to context
     * @param key city_name
     * @param values price values for key
     * @param context mapreduce context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int num = 0;
        for (IntWritable value : values) {
            if (value.get() > 250) {
                num++;
            }
        }
        context.write(key, new IntWritable(num));
    }
}
